package com.jprotechnologies.jpro.examples.webview1;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.web.WebView;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by TB on 23.03.17.
 */
public class WebView1Controlller implements Initializable
{
    @FXML
    private WebView webView;

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        webView.getEngine().load("http://www.bbc.com");
    }
}
