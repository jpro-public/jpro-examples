package com.jprotechnologies.jpro.examples.htmlnode;

import com.jpro.HTMLNode;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by TB on 23.03.17.
 */
public class HTMLNodeController implements Initializable
{
    @FXML
    public HTMLNode htmlNode;

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        //htmlNode.setHtml();
    }
}
