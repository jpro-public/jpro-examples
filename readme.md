# jpro examples


## Prerequisites

* Install [Gradle 3.x](https://gradle.org/)
* Define your jpro license credentials in jpro.conf file (src/main/resources)
```
 jpro.license {
     username = ""
     password = ""
     licensenumber = ""
 }
 ```


## Start Web version (HTML5/jpro)

### Development Mode
```
gradle stopServer runBrowser
```

### Production Mode (background mode)
```
gradle restartServer
```

Open in Browser: [http://localhost:9000](http://localhost:9000/index.html)


## Start Desktop version (JavaFX) 
 ```
 gradle run
 ```